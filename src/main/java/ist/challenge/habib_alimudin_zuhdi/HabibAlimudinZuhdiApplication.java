package ist.challenge.habib_alimudin_zuhdi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HabibAlimudinZuhdiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HabibAlimudinZuhdiApplication.class, args);
	}

}
