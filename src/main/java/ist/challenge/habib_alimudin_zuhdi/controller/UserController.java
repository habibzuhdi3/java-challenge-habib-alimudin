package ist.challenge.habib_alimudin_zuhdi.controller;

import ist.challenge.habib_alimudin_zuhdi.dto.request.SignRequest;
import ist.challenge.habib_alimudin_zuhdi.service.impl.UserServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class UserController {

    private final UserServiceImpl userService;

    @PostMapping(value = "/signup")
    public ResponseEntity<Object> signupUser(@RequestBody SignRequest signRequest){
        return userService.signup(signRequest);
    }

    @PostMapping(value = "/signin")
    public ResponseEntity<Object> signinUser(@RequestBody SignRequest request){
        return userService.signin(request);
    }

    @PutMapping(value = "/update/{id}/user")
    public ResponseEntity<Object> updateUser(@RequestBody SignRequest request, @PathVariable Long id){
        return userService.editUser(request, id);
    }
    @GetMapping(value = "/getAllUser")
    public ResponseEntity<Object> getAllUser(){
        return userService.getUser();
    }
}
