package ist.challenge.habib_alimudin_zuhdi.dto.request;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class SignRequest {

    private String username;

    private String password;

}
