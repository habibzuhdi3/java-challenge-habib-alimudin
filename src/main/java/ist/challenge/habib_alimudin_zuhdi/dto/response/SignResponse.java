package ist.challenge.habib_alimudin_zuhdi.dto.response;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
@Builder
public class SignResponse {

    private String username;

}
