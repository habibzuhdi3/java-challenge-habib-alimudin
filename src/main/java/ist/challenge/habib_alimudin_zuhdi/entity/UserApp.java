package ist.challenge.habib_alimudin_zuhdi.entity;


import ist.challenge.habib_alimudin_zuhdi.dto.response.SignResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class UserApp{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    @Size(max=25, message = "username max 25 characters")
    private String username;

    @Size(max = 25, message = "password max 25 characters")
    private String password;

    public SignResponse convertUserResponse(){
        return SignResponse.builder()
                .username(this.username).build();
    }
}
