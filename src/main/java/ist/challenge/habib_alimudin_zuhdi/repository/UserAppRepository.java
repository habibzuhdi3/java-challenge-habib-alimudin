package ist.challenge.habib_alimudin_zuhdi.repository;

import ist.challenge.habib_alimudin_zuhdi.entity.UserApp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAppRepository extends JpaRepository<UserApp, Long> {

    UserApp findByUsername(String username);

    Boolean existsByUsername(String username);
}
