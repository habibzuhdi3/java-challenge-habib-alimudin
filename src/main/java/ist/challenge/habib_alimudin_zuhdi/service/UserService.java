package ist.challenge.habib_alimudin_zuhdi.service;

import ist.challenge.habib_alimudin_zuhdi.dto.request.SignRequest;
import org.springframework.http.ResponseEntity;

public interface UserService {
    ResponseEntity<Object> signup(SignRequest signRequest);

    ResponseEntity<Object> editUser(SignRequest requestEdit, Long id);

    ResponseEntity<Object> signin(SignRequest request);

    ResponseEntity<Object> getUser();
}
