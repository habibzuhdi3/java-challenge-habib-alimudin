package ist.challenge.habib_alimudin_zuhdi.service.impl;

import ist.challenge.habib_alimudin_zuhdi.dto.request.SignRequest;
import ist.challenge.habib_alimudin_zuhdi.dto.response.SignResponse;
import ist.challenge.habib_alimudin_zuhdi.entity.UserApp;
import ist.challenge.habib_alimudin_zuhdi.exception.ResourceNotFoundException;
import ist.challenge.habib_alimudin_zuhdi.handler.ResponseHandler;
import ist.challenge.habib_alimudin_zuhdi.repository.UserAppRepository;
import ist.challenge.habib_alimudin_zuhdi.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional
public class UserServiceImpl implements UserService {

    private UserAppRepository userAppRepository;

    @Override
    public ResponseEntity<Object> signup(SignRequest signRequest){
        UserApp checkUser = this.userAppRepository.findByUsername(signRequest.getUsername());

        if(checkUser != null){
            return ResponseHandler.generateResponse("Username sudah terpakai", HttpStatus.CONFLICT, null);
        }

        UserApp newUser = new UserApp();
        newUser.setUsername(signRequest.getUsername());
        newUser.setPassword(signRequest.getPassword());

        this.userAppRepository.save(newUser);

        return ResponseHandler.generateResponse("Congratulations, account registration has been successful, please login for more access", HttpStatus.CREATED, newUser.convertUserResponse());
    }

    @Override
    public ResponseEntity<Object> editUser(SignRequest requestEdit, Long id){
        Optional<UserApp> checkAccount = this.userAppRepository.findById(id);


        if(checkAccount.isEmpty()){
//            throw new ResourceNotFoundException("User Tidak Ditemukan");
            return ResponseHandler.generateResponse("User dengan Id : "+id+", Tidak Ditemukan",HttpStatus.NOT_FOUND, null);

        }

        var username = checkAccount.get().getUsername();
        var pass = checkAccount.get().getPassword();

        if (requestEdit.getUsername().equals(username)){
            return ResponseHandler.generateResponse("Username sudah terpakai",HttpStatus.CONFLICT, null);
        }

        if (requestEdit.getPassword().equals(pass)){
            return ResponseHandler.generateResponse("Password tidak boleh sama dengan password sebelumnya",HttpStatus.CONFLICT, null);
        }

        UserApp editUser = new UserApp();
        editUser.setId(id);
        editUser.setUsername(requestEdit.getUsername());
        editUser.setPassword(requestEdit.getPassword());

        this.userAppRepository.save(editUser);

        return ResponseHandler.generateResponse("Username "+requestEdit.getUsername()+"'s Data Has Been Updated",HttpStatus.CREATED,editUser.convertUserResponse());
    }
    @Override
    public ResponseEntity<Object> signin(SignRequest request){

        if(request.getUsername().isEmpty() || request.getPassword().isEmpty()){
            return ResponseHandler.generateResponse("Username dan / atau password kosong", HttpStatus.BAD_REQUEST, null);
        }

        UserApp user = this.userAppRepository.findByUsername(request.getUsername());

        if(user == null){
            throw new ResourceNotFoundException("Username / Password Salah");
        }

        Boolean checkPassword = request.getPassword().equals(user.getPassword());
        if(Boolean.FALSE.equals(checkPassword)){
            throw new ResourceNotFoundException("Username / Password Salah");
        }

        return ResponseHandler.generateResponse("Sukses Login", HttpStatus.OK, null );
    }

    @Override
    public ResponseEntity<Object> getUser(){

        List<UserApp> listUser = this.userAppRepository.findAll();

        List<SignResponse> responUser = listUser.stream().map(UserApp::convertUserResponse).collect(Collectors.toList());

        return ResponseHandler.generateResponse("Get All User ", HttpStatus.OK, responUser);
    }

}
