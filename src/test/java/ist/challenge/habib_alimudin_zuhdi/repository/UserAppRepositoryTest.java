package ist.challenge.habib_alimudin_zuhdi.repository;

import ist.challenge.habib_alimudin_zuhdi.entity.UserApp;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class UserAppRepositoryTest {

    @Autowired
    private UserAppRepository underTest;

    @Test
    void testUsernameIsExist() {
        //given
        var username = "habibi";
        UserApp user = new UserApp(
                5L,
                username,
                "pass5647");
        underTest.save(user);

        //when
        UserApp found = underTest.findByUsername(username);

        boolean test = false;

        if(found != null){
            test = Boolean.TRUE;
        }

        //then
        assertThat(test).isTrue();
    }

    @Test
    void testUsernameIsNotExist() {
        var username = "alimudin";

        UserApp found = underTest.findByUsername(username);

        boolean test = false;

        if(found != null){
            test = Boolean.TRUE;
        }

        assertThat(test).isFalse();
    }

    @Test
    void checkShouldExistsByUsername() {

        var username = "alimudin";
        UserApp user = new UserApp(
                6L,
                username,
                "password123");
        underTest.save(user);

        boolean expect = underTest.existsByUsername(username);

        assertThat(expect).isTrue();

    }

    @Test
    void checkShouldNotExistsByUsername() {

        var username = "alimudin";

        boolean expect = underTest.existsByUsername(username);

        assertThat(expect).isFalse();

    }
}