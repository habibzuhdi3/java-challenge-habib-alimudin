package ist.challenge.habib_alimudin_zuhdi.service.impl;

import ist.challenge.habib_alimudin_zuhdi.repository.UserAppRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock private UserAppRepository userRepository;
    private UserServiceImpl underTest;

    @BeforeEach
    void setUp() {
        underTest = new UserServiceImpl(userRepository);
    }

    @Test
    @Disabled
    void signup() {
    }

    @Test
    @Disabled
    void editUser() {
    }

    @Test
    @Disabled
    void signin() {
    }

    @Test
    void getUser() {
        underTest.getUser();
        verify(userRepository).findAll();
    }
}